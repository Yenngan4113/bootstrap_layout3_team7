let mybutton = document.getElementById("btn-back-to-top");
var myNav = document.getElementById("mynav");
window.onscroll = function () {
  "use strict";
  if (document.documentElement.scrollTop >= 50) {
    myNav.classList.add("bg_theme");
    myNav.classList.add("nav_active");
    myNav.classList.remove("bg-transparent");
    // mybutton.style.display = "block";
  } else {
    myNav.classList.add("bg-transparent");
    myNav.classList.remove("bg_theme");
    myNav.classList.remove("nav_active");
    // mybutton.style.display = "none";
  }
};

document
  .querySelector("header .searchbar")
  .addEventListener("click", function (event) {
    document.querySelector(" header .searchdiv").classList.add("active");
    event.stopPropagation();
  });
document.querySelector("body").addEventListener("click", function () {
  document.querySelector(" header .searchdiv").classList.remove("active");
});

let r = document.querySelector(":root");
console.log(r);
document.querySelector(".fa-sun").addEventListener("click", function (event) {
  r.style.setProperty("--txt-color", "#000");
  r.style.setProperty("--bg-color", "#fff");

  document.querySelector(".fa-moon").style.display = "inline-block";
  document.querySelector(".fa-sun").style.display = "none";
  document.querySelector(".txt_farm").style.color = "#f4f4f4";
  event.stopPropagation();
});
document.querySelector(".fa-moon").addEventListener("click", function () {
  r.style.setProperty("--txt-color", "white");
  r.style.setProperty("--bg-color", "#404040");

  document.querySelector(".fa-moon ").style.display = "none";
  document.querySelector(".fa-sun").style.display = "inline-block";
  document.querySelector(".txt_farm").style.color = "#0b0d0e";
});
lightGallery(document.getElementById("gallery_test"), {
  selector: ".lightimg",
  plugins: [lgZoom, lgThumbnail, lgAutoplay, lgFullscreen, lgShare],
  alignThumbnails: "left",
  loop: true,
  allowMediaOverlap: true,
  toggleThumb: true,
  showZoomInOutIcons: true,
  actualSize: false,
  exThumbImage: "data-exthumbimage",
});

// // When the user clicks on the button, scroll to the top of the document
// mybutton.addEventListener("click", backToTop);

// function backToTop() {
//   document.body.scrollTop = 0;
//   document.documentElement.scrollTop = 0;
// }
// // dark  mode
// function myFunction() {
//   var element = document.body;
//   element.classList.toggle("dark-mode");
// }
